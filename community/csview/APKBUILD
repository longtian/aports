# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=csview
pkgver=1.2.4
pkgrel=0
pkgdesc="Pretty csv viewer for cli with cjk/emoji support"
url="https://github.com/wfxr/csview"
arch="all"
license="Apache-2.0 OR MIT"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/wfxr/csview/archive/v$pkgver/csview-$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	install -D -m644 completions/bash/$pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname

	install -D -m644 completions/fish/* -t "$pkgdir"/usr/share/fish/vendor_completions.d/
	install -D -m644 completions/zsh/* -t "$pkgdir"/usr/share/zsh/site-functions/
}

sha512sums="
58cd598ca4591906016cb32d812ec1e822617561a80170bf6066ffcb9509d1cf6f0e766573c7cee28ebc070018e32b6d047d301a3878bbca5dfaaa4e0d3effb6  csview-1.2.4.tar.gz
"
